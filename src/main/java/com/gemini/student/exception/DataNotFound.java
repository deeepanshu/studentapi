package com.gemini.student.exception;

public class DataNotFound extends Exception {
    public DataNotFound() {
        super("Data not found");
    }

    public DataNotFound(String message) {
        super(message);
    }
}
