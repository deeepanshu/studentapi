package com.gemini.student.exception;

public class DataAlreadyExists extends Exception {
    public DataAlreadyExists() {
        super("Data not found");
    }

    public DataAlreadyExists(String message) {
        super(message);
    }
}
