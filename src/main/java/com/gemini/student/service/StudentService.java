package com.gemini.student.service;

import com.gemini.student.exception.DataAlreadyExists;
import com.gemini.student.exception.DataNotFound;
import com.gemini.student.model.Student;
import com.gemini.student.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        List<Student> studentRecords = new ArrayList<Student>();
        studentRepository.findAll().forEach(studentRecords::add);
        return studentRecords;
    }

    public Optional<Student> getStudent(String id) {
        return studentRepository.findById(id);
    }

    public Student addStudent(Student student) throws DataAlreadyExists {
        Optional<Student> stu = this.getStudent(student.getRollNumber());
        if (stu.isPresent()) {
            throw new DataAlreadyExists();
        }
        return studentRepository.save(student);
    }

    public Student delete(String rollNumber) throws DataNotFound {
        Optional<Student> student = studentRepository.findById(rollNumber);
        if (!student.isPresent()) {
            throw new DataNotFound();
        }
        studentRepository.deleteById(rollNumber);
        return student.get();
    }

    public Student updateStudent(Student student) throws DataNotFound {
        Optional<Student> stu = this.getStudent(student.getRollNumber());
        if(!stu.isPresent()) {
            throw new DataNotFound();
        }
        return studentRepository.save(student);
    }

}
