package com.gemini.student.controller;

import com.gemini.student.exception.DataAlreadyExists;
import com.gemini.student.exception.DataNotFound;
import com.gemini.student.model.Student;
import com.gemini.student.service.StudentService;
import com.gemini.student.utils.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {

    private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public ApiResponse<List<Student>> getAllUsers() {
        List<Student> students;
        ApiResponse<List<Student>> apiResponse = null;
        try {
            students = studentService.getAllStudents();
            apiResponse = new ApiResponse<>(null, true, students, "Success");
        } catch (Exception e) {
            apiResponse = new ApiResponse<>(e.getMessage(), false, null, "Error Occurred");
            System.out.println(e);
        }
        return apiResponse;
    }

    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public ApiResponse<Student> addStudent(@RequestBody Student student) {
        ApiResponse<Student> apiResponse = null;
        try {
            Student stu = studentService.addStudent(student);
            apiResponse = new ApiResponse<Student>(null, true, stu, "Student Added");
        } catch (DataAlreadyExists dae) {
            apiResponse = new ApiResponse<Student>(dae.getMessage(), false, null, "Student already exists.");
        } catch (Exception e) {
            System.out.println(e);
        }
        return apiResponse;
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.GET)
    public ApiResponse<Student> getStudent(@PathVariable String id) {
        ApiResponse<Student> apiResponse = null;
        try {
            Optional<Student> student = studentService.getStudent(id);
            if (!student.isPresent()) {
                throw new DataNotFound();
            }
            apiResponse = new ApiResponse<Student>(null, true, student.get(), "Student found!");
        } catch (DataNotFound de) {
            apiResponse = new ApiResponse<Student>(de.getMessage(), false, null, "Student not found!");
        } catch (Exception e) {
            System.out.println(e);
        }
        return apiResponse;
    }

    @RequestMapping(value = "/student/{id}", method = RequestMethod.DELETE)
    public ApiResponse<Student> deleteStudent(@PathVariable String id) {
        ApiResponse<Student> apiResponse = null;
        try {
            Student student =  studentService.delete(id);
            apiResponse = new ApiResponse<Student>(null, true, student, "Student Deleted!");
        } catch (DataNotFound dnf) {
            apiResponse = new ApiResponse<Student>(dnf.getMessage(), false, null, "Student not found!");
        } catch (Exception e) {
            System.out.println(e);
        }
        return apiResponse;
    }

    @RequestMapping(value= "/student/{id}", method = RequestMethod.PUT)
    public ApiResponse<Student> updateStudent(@PathVariable String id, @RequestBody Student student) {
        student.setRollNumber(id);
        ApiResponse<Student> apiResponse = null;
        try {
            Student stu= studentService.updateStudent(student);
            apiResponse = new ApiResponse<Student>(null, true, stu, "Student Updated!");
        } catch (DataNotFound dnf) {
            apiResponse = new ApiResponse<Student>(dnf.getMessage(), false, null, "Student not found!");
        }
        return apiResponse;
    }
}
